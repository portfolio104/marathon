<?php

namespace App\Http\Controllers;
use App\Models\Serie;
use Illuminate\Http\Request;

class RechercheController extends Controller
{
    public function search(Request $request) {
        $q = request()->input('query');

        $series= Serie::where('nom','like',"%$q%") -> simplePaginate(10);

        return view('series.search', ['series' => $series]);

        
    }
    //
}
