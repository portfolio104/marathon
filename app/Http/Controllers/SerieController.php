<?php

namespace App\Http\Controllers;


use App\Models\Episode;
use App\Models\Serie;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;




class SerieController extends Controller
{

    public function index() {

        $series = DB::table('series')->simplePaginate(5);
        $genres=DB::table('series')
        ->select('series.genre', DB::raw('count(series.genre) as genre2'))
            ->groupBy('series.genre')
            ->orderBy('genre2','desc')
            ->get();
        return view('layouts.listeSerie', ['series'=>$series , 'genres'=>$genres]);

    }

    public function show($id) {
        $serie = Serie::findOrFail($id);

        $episodes = Episode::all()->where('serie_id','=',$id);
        $var = true;
        // ici on teste si tout les episodes de la série sont vue , on retourne donc oui ou non
        foreach ($episodes as $episode){

            $verif = DB::select('select episode_id from seen where user_id = ? and episode_id = ?', [Auth()->id(),$episode->id]);
            $verifEpisode = DB::select('select id from episodes where serie_id = ? and id = ?',[$id,$episode->id]);

            if(count($verif)>0 && count($verifEpisode)>0){
                if($verif[0]->episode_id != $verifEpisode[0]->id){

                    $var = false;
                }
            }
            else{
                $var = false;
            }

        }
        $tab = [];
        $saisons = Episode::all()->where('serie_id','=',$id);
        foreach ($saisons as $saison){
            $tab[$saison->saison]=[];
        }


        for( $i = 1 ; $i < count($tab) +1 ; $i++){

            $episodesDeLaSerie = Episode::all()
                ->where('serie_id','=',$id)
                ->where('saison','=',$i);

            $episodesVueDeLaSerie = DB::table('episodes')
                ->join('seen','episodes.id','=','seen.episode_id')
                ->where('user_id','=',Auth()->id())
                ->where('serie_id','=',$id)
                ->get('episode_id');


            // me permet de recuperer les episodes vues $episodesVueDeLaSerie->get('episodes_id');


            foreach ($episodesDeLaSerie as $episodeS){
                $episodeVue = '0';
                foreach ($episodesVueDeLaSerie as $episodeV){
                    if ($episodeS->id == $episodeV->episode_id){
                        $episodeVue = '1';
                    }
                }

                if ($episodeVue == '0'){
                    $tab[$i][]='0';
                }
                else{
                    $tab[$i][]=$episodeS->id;

                }
            }
        }
        return view('layouts.detailsSerie', ['serie'=>$serie,'commentaires'=>$serie->comments()->get(),'episodes'=>$serie->episodes()->get(),'var'=>$var,'tab'=>$tab]);

    }

    public function ajouteCommentaire(Request $request, $serie){
        $texte = "<p>".$request->commentaire."</p>";
        $dt = new DateTime();
        $dt = date("d-m-Y H:i:s");
        $commentaire = DB::table('comments')->insert(array('content' => $texte,'note'=>$request->note,'validated'=>0,'user_id'=>Auth()->id(),'serie_id'=>$serie,'created_at'=>$dt,'updated_at'=>$dt));
        return redirect(route('afficheDetail',$serie)) ;

    }

    public function valideCommentaire($id){
        $commentaire = DB::table('comments')
            ->where('id','=',$id)
            ->update(['validated'=>1]);

        $serieId = DB::table('comments')
            ->where('id','=',$id)
            ->get('serie_id');
        return redirect(route('afficheDetail',$serieId[0]->serie_id)) ;
    }
    public function selectGenre(Request $request){
        $series = DB::table('series')->get();
        $genres=DB::table('series')
            ->select('series.genre', DB::raw('count(series.genre) as genre2'))
            ->groupBy('series.genre')
            ->orderBy('genre2','desc')
            ->get();
        $search_genre=$request->input('genreSelection');
        $serieParGenre=DB::table('series')->where('genre','=',$search_genre)->get();
        return view('layouts.listeSerie', ['series'=>$series , 'genres'=>$genres,'serieParGenre'=>$serieParGenre] );

    }
    public function trieParGenre($nom){
        $series = DB::table('series')->get();
        $genres=DB::table('series')
            ->select('series.genre', DB::raw('count(series.genre) as genre2'))
            ->groupBy('series.genre')
            ->orderBy('genre2','desc')
            ->get();
        $serieParGenre=DB::table('series')->where('genre','=',$nom)->get();
        return view('layouts.listeSerie', ['series'=>$series , 'genres'=>$genres,'serieParGenre'=>$serieParGenre] );

    }

}
