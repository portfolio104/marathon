<?php

namespace App\Http\Controllers;

use App\Models\Episode;
use App\Models\Serie;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VisualiseController extends Controller
{
    public function serieVue(Request $request, $id){

        if($request->serieVue == 'oui'){

            $episodes = Episode::all()->where('serie_id','=',$id);
            $dt = new DateTime();
            $dt->format('Y-m-d');

            $var = false;


            foreach ($episodes as $episode){

                $verif = DB::select('select * from seen where user_id = ? and episode_id = ?', [Auth()->id(),$episode->id]);
                if(empty($verif)){
                    $visualise = DB::table('seen')->insert(array('user_id' => Auth()->id(),'episode_id'=>$episode->id,'date_seen'=>$dt->format('Y-m-d')));
                    $var = true;
                }
                else{
                    $var = true;
                }
            }
        }

        else{

            $episodes = Episode::all()->where('serie_id','=',$id);
            $dt = new DateTime();
            $dt->format('Y-m-d');

            foreach ($episodes as $episode){
                $visualise = DB::table('seen')
                    ->where('user_id','=',Auth()->id())
                    ->where('episode_id','=',$episode->id)
                    ->delete();

            }

            $var = false;

        }

        $serie = Serie::findOrFail($id);
        return redirect(route('afficheDetail',$id)) ;
    }

    public function saisonVue(Request $request, $id, $saison){
        if($request->saisonVue == 'oui'){

            $episodes = Episode::all()
                ->where('serie_id','=',$id)
                ->where('saison','=',$saison);

            $varSaison = false;

            $dt = new DateTime();
            $dt->format('Y-m-d');
            foreach($episodes as  $episode){

                $verif = DB::select('select * from seen where user_id = ? and episode_id = ?', [Auth()->id(),$episode->id]);
                if(empty($verif)){
                    $visualise = DB::table('seen')->insert(array('user_id' => Auth()->id(),'episode_id'=>$episode->id,'date_seen'=>$dt->format('Y-m-d')));
                }
                $varSaison = true;
            }

        }
        else{

            $episodes = Episode::all()
                ->where('serie_id','=',$id)
                ->where('saison','=',$saison);


            foreach ($episodes as $episode){
                $visualise = DB::table('seen')
                    ->where('user_id','=',Auth()->id())
                    ->where('episode_id','=',$episode->id)
                    ->delete();

            }

            $varSaison = false;
        }

        $serie = Serie::findOrFail($id);
        //return view('layouts.detailsSerie', ['serie'=>$serie,'commentaires'=>$serie->comments()->get(),'episodes'=>$serie->episodes()->get(),'varSaison'=>$saison]);
        return redirect(route('afficheDetail',$id)) ;
    }

    public function episodeVue(Request $request, $id, $saison, $episode){

        if($request->episodeVue == 'oui'){

            $episodes = Episode::all()
                ->where('serie_id','=',$id)
                ->where('saison','=',$saison)
                ->where('id','=',$episode);

            $varEpisode = false;

            $dt = new DateTime();
            $dt->format('Y-m-d');
            foreach($episodes as  $episode){

                $verif = DB::select('select * from seen where user_id = ? and episode_id = ?', [Auth()->id(),$episode]);
                if(empty($verif)){
                    $visualise = DB::table('seen')->insert(array('user_id' => Auth()->id(),'episode_id'=>$episode->id,'date_seen'=>$dt->format('Y-m-d')));
                }
                $varEpisode = true;
            }

        }
        else{

            $episodes = Episode::all()
                ->where('serie_id','=',$id)
                ->where('saison','=',$saison)
                ->where('id','=',$episode);



            foreach ($episodes as $episode){
                $visualise = DB::table('seen')
                    ->where('user_id','=',Auth()->id())
                    ->where('episode_id','=',$episode->id)
                    ->delete();

            }

            $varSaison = false;
        }

        $serie = Serie::findOrFail($id);
        return redirect(route('afficheDetail',$id)) ;

    }

}
