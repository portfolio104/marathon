<?php

namespace App\Http\Controllers;

use App\Models\Episode;
use App\Models\Serie;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AcceuilController extends Controller
{
    public function index(){

        $seriesMieuxNote = Serie::all()->where('note','>',8)->take(5); // retourne les valeurs de notes > 9
        $seriesDerniereSortie = Serie::all()->where('premiere','>','2021-01-01')->take(5); // retourne les valeurs de notes > 9
        $test= DB::table('seen')
            ->join('episodes','seen.episode_id','=','episodes.id')
            ->join('series','episodes.serie_id','=','series.id')
            ->select('series.*', DB::raw('count(series.nom) as nom2'))
            ->groupBy('series.id')
            ->orderBy('nom2','desc')
            ->get();
        return view('pageAcceuil',['seriesMieuxNote'=>$seriesMieuxNote,'seriesDerniereSortie'=>$seriesDerniereSortie,'test'=>$test]);

    }
}
