<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Episode;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function afficheUser()
    {
        $serieVue= DB::table('seen')
            ->select(DB::raw('seen.episode_id'))
            ->where('user_id','=',Auth::user()->id)
            ->join('episodes','seen.episode_id','=','episodes.id')
            ->select('episodes.serie_id')
            ->join('series','episodes.serie_id','=','series.id')
            ->select('series.*')
            ->groupBy('series.id')
            ->get();

        $commentairesAValider=Comment::all();
        return view('profil.InfoUser',['serieVue'=>$serieVue,'commentairesAValider'=> $commentairesAValider]);

    }

    public function donneEpisodeSuivant($serie){
        // $vue donne les episodes deja vu de la série
        $episodesDeLaSerie = Episode::all()
            ->where('serie_id','=',$serie);

        $episodesVueDeLaSerie = DB::table('episodes')
            ->join('seen','episodes.id','=','seen.episode_id')
            ->where('user_id','=',Auth()->id())
            ->where('serie_id','=',$serie)
            ->get('episode_id');


        foreach ($episodesDeLaSerie as $episodeS){
            $trouve = -1;
            foreach ($episodesVueDeLaSerie as $episodeV){
                if($episodeV->episode_id != $episodeS->id ){
                    $trouve = $episodeV;
                }
            }

        }

        return $episodesVueDeLaSerie;

    }

    
}

