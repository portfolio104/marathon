@extends('base.base')
@section('tittle')
    Profil
@endsection
@section('content')
    {{--    <h2>Information utilisateur</h2>
        <table>
            <tr>
                <ol><strong>id: {{Auth::user()->id}}</strong></ol>
    <ol><strong>Nom: {{Auth::user()->name}}</strong></ol>
    <ol><strong>Mail: {{Auth::user()->email}}</strong></ol>
    <ol><strong>Avatar: {{Auth::user()->avatar}}</strong></ol>

    </tr>
    </table>

    <h2>Liste des séries vues</h2>
    <table>
        @foreach($serieVue as $serieV)
            <tr>
                <td>{{$serieV->nom}}</td>
                <td><img src="{{$serieV->urlImage}}"></td>

            </tr>
        @endforeach
    </table>




    --}}
<section class="profil">
    <div class="details-profil">
        <img src="{{Auth::user()->avatar}}" alt="avatar profil">

        <h2>{{Auth::user()->name}}</h2>

        <p>{{Auth::user()->email}}</p>

        <form  action="{{ route('logout') }}" method="POST" >
            {{ csrf_field() }}
            <input type="submit" value="Se deconnecter">
        </form>
    </div>


    <div class="all-series">
        @foreach($serieVue as $serieV)
        <div class="serie-solo-profil">
            <img src="{{$serieV->urlImage}}" alt="cover de la série">
        </div>
        @endforeach
    </div>
    @if(Auth::user()->administrateur=="1")
        <button id='seeCom' class="sup">voir les commentaires</button>
        <div class="comment-section">
        <div class="all-comments">
        <div class="validate-comments">
            <h4>COMMENTAIRES A VALIDER</h4>

            <ul>
        @foreach($commentairesAValider as $com)
            @if(!$com->validated)
                        <li>
                            {!!Str::limit($com->content,144)!!}
                        </li>
            @endif
        @endforeach
            </ul>
        </div>
        </div>
        </div>
    @endif

</section>
@endsection