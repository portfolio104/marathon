@extends('base.base')
@section('tittle')
    Accueil
@endsection
@section('content')

    {{--
    <section>
        <h1>les films</h1>

        <div class="last_series">
            <h2>les dernieres sorties</h2>

            <div class="last-series-container">

                @foreach($seriesDerniereSortie as $serieSortie)

                <div class="last_serie">
                        <h3>{{$serieSortie->nom}}</h3>

                        <div class="last-cover-serie">
                            <img src="{{$serieSortie->urlImage}}" alt="">
                        </div>

                        <div class="date-link-more">
                            <p class="date-sortie">{{$serieSortie->premiere}}</p>
                            <a href="{{ url("/details{$serieSortie->id}") }}">+</a>

                        </div>
                    </div>
                @endforeach


            </div>

        </div>

        <div class="best_seen">
            <h2>les mieux notées</h2>

            <div class="best-seen-container">
                @foreach($seriesMieuxNote as $serieN)
                    <div class="seen_serie">
                        <img src="{{$serieN->urlImage}}" alt="">

                        <ul>
                            @for($i=0; $i<$serieN->note;$i+=2)
                                <li><img src="img/icons/stars.svg" alt=""></li>
                            @endfor
                        </ul>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="best_seen">
            <h2>les plus vue</h2>

            <div class="best-seen-container">
                {{$i=0}}
                @foreach($test as $t)
                    {{$i++}}
                    <div class="seen_serie">
                        <img src="{{$t->urlImage}}" alt="">
                    </div>
                    @if($i>=5)
                        @break
                    @endif
                @endforeach
            </div>
        </div>

        <div class="link-series-more">
            <a href="">Voir plus de série</a>
        </div>
    </section>
    --}}


    <section id="index">


        <div class="last_series">
            <h2>les dernieres sorties</h2>


            <div class="last-series-container">

                @foreach($seriesDerniereSortie as $serieSortie)
                    <div class="last_serie">
                        <h3>{{$serieSortie->nom}}</h3>

                        <div class="last-cover-serie">
                            <img src="{{$serieSortie->urlImage}}" alt="cover du film">
                        </div>

                        <div class="date-link-more">
                            <p class="date-sortie">{{$serieSortie->premiere}}</p>
                            <a href="{{url("/details{$serieSortie->id}") }}">...</a>

                        </div>
                    </div>
                @endforeach
            </div>

        </div>

        <div class="seen_series">
            <h2 id="title-seen-series">Les plus visionnées</h2>
            <div class="seen-serie-container">
                {{$i=0}}
                @foreach($test as $t)
                    {{$i++}}
                    <a href="details{{$t->id}}">
                        <div class="seen_serie">
                            <img src="{{$t->urlImage}}" alt="">
                        </div>
                    </a>
                    @if($i>=5)
                        @break
                    @endif
                @endforeach
            </div>
        </div>



        <div class="best_note">
            <h2>les mieux notées</h2>

            <!-- LIENS VERS PLUS DE SERIES NOTÉES 10 ETOILES -->
            <a href="">
                <div class="best-note-container">
                    @foreach($seriesMieuxNote as $serieN)
                        <a class="link-best-note" href="/details{{$serieN->id}}">
                            <div class="note_serie" data-tilt data-tilt-max="20" data-tilt-speed="10" data-tilt-perspective="1500" data-tilt-glare data-tilt-max-glare="0.8">
                                <img src="{{$serieN->urlImage}}" alt="">


                                <ul>
                                    @for($i=0; $i<$serieN->note;$i+=2)
                                        <li><img src="img/icons/stars.svg" alt=""></li>
                                    @endfor
                                </ul>
                            </div>
                        </a>
                    @endforeach
                </div>
            </a>
        </div>

        <div class="link-series-more">
            <a href="/ListeSerie" data-tilt data-tilt-glare data-tilt-max-glare="0.8" data-tilt-scale="1.05">Voir plus de série -></a>
        </div>
    </section>
@endsection


