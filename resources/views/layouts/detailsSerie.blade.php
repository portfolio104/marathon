@extends('base.base')
@section('tittle')
    Détails
@endsection
@section('content')


    <section class="serie-details">
        @if(!empty($serie))
            <div class="serie-details-container">
                    <div class="serie-details-img-title">
                        <h1>{{$serie->nom}}</h1>
                        <img src="{{$serie->urlImage}}" alt="">
                    </div>

                    <div class="serie-resume-container">
                        <ul>
                            @for($i=0; $i<$serie->note;$i+=2)
                                <li><img src="img/icons/stars.svg" alt=""></li>
                            @endfor
                        </ul>
                        <h2>SYNOPISIS</h2>
                        {!! $serie->resume !!}
                        <p>{{$serie->langue}}</p>
                        <p>{{$serie->genre}}</p>
                        <p>{{$serie->premiere}}</p>
                    </div>
            </div>

            <div class="serie-details-comments">
                @if(Auth()->user())
                    <form action="/ajoutSerie{{$serie->id}}" method="POST">
                        {!! csrf_field() !!}
                        <p>Avez vous vue cette série:</p>

                        @if($var ?? '' == true)
                            <div>
                                <input type="radio" id="oui" name="serieVue" value="oui"
                                       checked>
                                <label for="oui">OUI</label>
                            </div>

                            <div>
                                <input type="radio" id="non" name="serieVue" value="non">
                                <label for="non">NON</label>
                            </div>

                        @else
                            <div>
                                <input type="radio" id="oui" name="serieVue" value="oui">
                                <label for="oui">OUI</label>
                            </div>

                            <div>
                                <input type="radio" id="non" name="serieVue" value="non"
                                       checked>
                                <label for="non">NON</label>
                            </div>

                        @endif
                        <input type="submit" value="Envoyer !">
                    </form>
                @endif
                @php
                    $saison = -1;
                    $episodeVal= 0;
                @endphp
                @foreach($episodes as $episode)
                    @if($episode->numero==1 and $episode->saison ==1)
                            <p>Affichage commentaire fin de page :</p>
                        <td><button id='seeCom'>voir les commentaires</button></td>

                    @endif


                    <table>

                    <div class="comments-title">
                        <h4>Saison n°{{$episode->saison}}</h4>

                        @if(Auth()->user() and $episode->numero==1)
                            <form action="/ajoutSerie{{$serie->id}}/{{$episode->saison}}" method="POST">
                                {!! csrf_field() !!}
                                <p>Avez vous vue cette saison:</p>
                                @if($var ?? '' == true or in_array('0',$tab[$episode->saison])==false)
                                    <div>
                                        <input type="radio" id="oui" name="saisonVue" value="oui"
                                               checked>
                                        <label for="oui">OUI</label>
                                    </div>

                                    <div>
                                        <input type="radio" id="non" name="saisonVue" value="non">
                                        <label for="non">NON</label>
                                    </div>

                                @else
                                    <div>
                                        <input type="radio" id="oui" name="saisonVue" value="oui">
                                        <label for="oui">OUI</label>
                                    </div>

                                    <div>
                                        <input type="radio" id="non" name="saisonVue" value="non"
                                               checked>
                                        <label for="non">NON</label>
                                    </div>

                                @endif

                                <input type="submit" value="Envoyer !">
                            </form>
                        @endif
                    </div>


                        <tr class="serie-details-form">
                        <td>Episode n°{{$episode->numero}}</td>
                        <td>
                            @if(Auth()->user())
                                <form action="/ajoutSerie{{$serie->id}}/{{$episode->saison}}/{{$episode->id}}" method="POST">
                                    {!! csrf_field() !!}
                                    <p>Avez vous vue cet épisode:</p>

                                    @if($var ?? '' == true or in_array($episode->id,$tab[$episode->saison])==true)
                                        <div>
                                            <input type="radio" id="oui" name="episodeVue" value="oui"
                                                   checked>
                                            <label for="oui">OUI</label>
                                        </div>

                                        <div>
                                            <input type="radio" id="non" name="episodeVue" value="non">
                                            <label for="non">NON</label>
                                        </div>

                                    @else
                                        <div>
                                            <input type="radio" id="oui" name="episodeVue" value="oui">
                                            <label for="oui">OUI</label>
                                        </div>

                                        <div>
                                            <input type="radio" id="non" name="episodeVue" value="non"
                                                   checked>
                                            <label for="non">NON</label>
                                        </div>

                                    @endif

                                    <input type="submit" value="Envoyer !">
                                </form>
                            @endif
                        </td>
                    </tr>

                    @endforeach
                </table>

                <div class="comment-section">
                    <div class="all-comments">
                        <h3>COMMENTAIRES</h3>
                        <ul>
                            @foreach($commentaires as $commentaire)
                                @if(Auth()->user())
                                    @if(Auth()->user()->administrateur == 1)
                                        <li>{!! $commentaire->content !!}</li>
                                    @else
                                        @if($commentaire->validated == "1")
                                            <li>{!! $commentaire->content !!}</li>
                                        @endif
                                    @endif
                                @else
                                    <li>{!! $commentaire->content !!}</li>
                                @endif
                            @endforeach


                        </ul>
                    </div>
                    @if(Auth()->user())
                        @if(Auth()->user()->administrateur==1)
                            <div class="validate-comments">

                                <h4>COMMENTAIRES A VALIDER</h4>


                                <ul>
                                    @foreach($commentaires as $commentaire)
                                        @if(Auth()->user())
                                            @if(Auth()->user()->administrateur == 1 && $commentaire->validated == "0")
                                                <li>{!! $commentaire->content !!}</li>
                                                <li>
                                                    <a href="/valideCommentaire/{{$commentaire->id}}">Valider le commentaire</a>
                                                </li>
                                            @endif

                                        @else
                                            <li>{!! $commentaire->content !!}}</li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    @endif

                </div>
                        @if(Auth()->user())
                            <div class="add-com">
                                <form action="/ajoutCommentaire{{$serie->id}}" method="POST">
                                    {!! csrf_field() !!}
                                    <p>Déposer un commentaire sur la série</p>
                                    <label for="note">Saissisez votre note pour la série :</label>
                                    <input type="number" id="note" name="note" step="0.1" min="0" max="10">
                                    <label for="commentaire">Saissisez votre commentaire sur la série :</label>
                                    <textarea id="commentaire" name="commentaire" rows="5" cols="33"required>Votre commentaire</textarea>
                                    <input type="submit" value="Envoyer !">
                                </form>
                            </div>

                        @endif
            </div>


        @endif




    </section>


    {{--

    @if(!empty($serie))
        <h2>Details Serie</h2>
        <table>
            <tr>
                <th></th>
                <th>nom</th>
                <th>genre</th>
                <th>langue</th>
                <th>date de sortie</th>
                <th>avis</th>
            </tr>



            @if(Auth()->user())
                <form action="/ajoutSerie{{$serie->id}}" method="POST">
                    {!! csrf_field() !!}
                    <p>Avez vous vue cette série:</p>

                    @if($var ?? '' == true)
                        <div>
                            <input type="radio" id="oui" name="serieVue" value="oui"
                                   checked>
                            <label for="oui">OUI</label>
                        </div>

                        <div>
                            <input type="radio" id="non" name="serieVue" value="non">
                            <label for="non">NON</label>
                        </div>

                    @else
                        <div>
                            <input type="radio" id="oui" name="serieVue" value="oui">
                            <label for="oui">OUI</label>
                        </div>

                        <div>
                            <input type="radio" id="non" name="serieVue" value="non"
                                   checked>
                            <label for="non">NON</label>
                        </div>

                    @endif
                    <input type="submit" value="Envoyer !">
                </form>
            @endif



            <tr>
                <td><img src="{{$serie->urlImage}}"></img></td>
                <td>{{$serie->nom}}</td>
                <td>{{$serie->genre}}</td>
                <td>{{$serie->langue}}</td>
                <td>{{$serie->premiere}}</td>
                <td>{{$serie->avis}}</td>

            </tr>

            @foreach($commentaires as $commentaire)
                @if(Auth()->user())
                    @if(Auth()->user()->administrateur == 1)
                        <td>{!! $commentaire->content !!}}</td>
                    @else
                        @if($commentaire->validated == "1")
                            <td>{!! $commentaire->content !!}}</td>
                        @endif
                    @endif

                    @if(Auth()->user()->administrateur == 1 && $commentaire->validated == "0")
                            <td>
                                <a href="/valideCommentaire/{{$commentaire->id}}">Valider le commentaire</a>
                            </td>
                        @endif

                @else
                    <td>{!! $commentaire->content !!}}</td>
                @endif




            @endforeach
            @php
                $saison = -1;
                $episodeVal= 0;
            @endphp
            @foreach($episodes as $episode)
                <tr>

                    @if(Auth()->user())
                        <td>{{$episode->saison}}</td>
                        <td>{{$episode->numero}}</td>
                    @endif

                    <td>
                        @if($saison != $episode->saison)
                            @if(Auth()->user())
                                <form action="/ajoutSerie{{$serie->id}}/{{$episode->saison}}" method="POST">
                                    {!! csrf_field() !!}
                                    <p>Avez vous vue cette saison:</p>
                                    @if($var ?? '' == true or in_array('0',$tab[$episode->saison])==false)
                                        <div>
                                            <input type="radio" id="oui" name="saisonVue" value="oui"
                                                   checked>
                                            <label for="oui">OUI</label>
                                        </div>

                                        <div>
                                            <input type="radio" id="non" name="saisonVue" value="non">
                                            <label for="non">NON</label>
                                        </div>

                                    @else
                                        <div>
                                            <input type="radio" id="oui" name="saisonVue" value="oui">
                                            <label for="oui">OUI</label>
                                        </div>

                                        <div>
                                            <input type="radio" id="non" name="saisonVue" value="non"
                                                   checked>
                                            <label for="non">NON</label>
                                        </div>

                                    @endif

                                    <input type="submit" value="Envoyer !">
                                </form>
                            @endif

                            @php
                                $saison = $episode->saison;
                            @endphp

                        @endif

                    </td>

                    <td>
                        @if(Auth()->user())
                            <form action="/ajoutSerie{{$serie->id}}/{{$episode->saison}}/{{$episode->id}}" method="POST">
                                {!! csrf_field() !!}
                                <p>Avez vous vue cet épisode:</p>

                                @if($var ?? '' == true or in_array($episode->id,$tab[$episode->saison])==true)
                                    <div>
                                        <input type="radio" id="oui" name="episodeVue" value="oui"
                                               checked>
                                        <label for="oui">OUI</label>
                                    </div>

                                    <div>
                                        <input type="radio" id="non" name="episodeVue" value="non">
                                        <label for="non">NON</label>
                                    </div>

                                @else
                                    <div>
                                        <input type="radio" id="oui" name="episodeVue" value="oui">
                                        <label for="oui">OUI</label>
                                    </div>

                                    <div>
                                        <input type="radio" id="non" name="episodeVue" value="non"
                                               checked>
                                        <label for="non">NON</label>
                                    </div>

                                @endif

                                <input type="submit" value="Envoyer !">
                            </form>
                        @endif
                    </td>

                </tr>
            @endforeach



        </table>

        @if(Auth()->user())
            <form action="/ajoutCommentaire{{$serie->id}}" method="POST">
                {!! csrf_field() !!}
                <p>Déposer un commentaire sur la série</p>
                <label for="note">Saissisez votre note pour la série :</label>
                <input type="number" id="note" name="note" step="0.1" min="0" max="10">
                <label for="commentaire">Saissisez votre commentaire sur la série :</label>
                <textarea id="commentaire" name="commentaire" rows="5" cols="33"required>
                    Votre commentaire
                </textarea>
                <input type="submit" value="Envoyer !">
            </form>
        @endif


    @else
        <h3>aucune serie</h3>
    @endif
    --}}

@endsection