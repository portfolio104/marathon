@if ($paginator->hasPages())

    @if ($paginator->onFirstPage())

        <span class="btn_paginate2">
            @lang('Précédente')
        </span>

    @else

        <a href=" {{ $paginator->previousPageUrl() }} " class="btn_paginate">
            @lang('Précédente')
        </a>

    @endif

    @if ($paginator->hasMorePages())

        <a href=" {{ $paginator->nextPageUrl() }} " class="btn_paginate">
            @lang('Suivante')
        </a>

    @else

        <span class="btn_paginate2">
            @lang('Suivante')
        </span>

    @endif

@endif