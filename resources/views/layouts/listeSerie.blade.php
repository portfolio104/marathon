@extends('base.base')
@section('tittle')
    Liste
@endsection
@section('content')
    <section>
        <h1>TOUTES LES SERIES </h1>
        <form method="get" action="{{ route("test") }}">
            <select name="genreSelection">
                @foreach($genres as $genre)
                    <option value="{{$genre->genre}}">{{$genre->genre}}</option>
                @endforeach
            </select>
            <button type="submit">Go</button>
        </form>
        <div class="series-container">

            @if(!empty($series))

                @if(empty($serieParGenre))
                    @foreach($series as $serie)

                        <a href="/details{{$serie->id}}">
                            <div class="serie" data-tilt data-tilt-glare data-tilt-max-glare="0.8">
                                <h3>{{$serie->nom}}</h3>
                                <div class="cover-serie">
                                    <img src="{{$serie->urlImage}}" alt="cover du film">
                                </div>
                                <p><a href="{{ url("/trierParGenreClick{$serie->genre}")  }}">{{$serie->genre}} </a></p>
                                <p>{{$serie->langue}}</p>
                                <p>{{$serie->note}}</p>
                            </div>
                        </a>
                    @endforeach
                        <div class="paginate">
                            {{$series->links('layouts.paginator')}}
                        </div>
                @endif
                @if(!empty($serieParGenre))
                    @foreach($serieParGenre as $spg)
                        <a href="{{ url("/details{$spg->id}") }}">
                            <div class="serie" data-tilt data-tilt-glare data-tilt-max-glare="0.8">
                                <h3>{{$spg->nom}}</h3>
                                <div class="cover-serie">
                                    <img src="{{$spg->urlImage}}">
                                </div>
                                <p><a href="{{ url("/genre") }}">{{$spg->genre}} </a></p>
                                <p>{{$spg->langue}}</p>
                                <p>{{$spg->note}}</p>
                            </div>
                        </a>
                    @endforeach
                @endif
            @endif



        </div>


    </section>

    {{--
    @if(!empty($series))
        <table>
            <tr>
                <th></th>
                <th>nom</th>
                <th>genre</th>
                <th>langue</th>
                <th>note</th>
                <th>
                    <form method="get" action="{{ route("test") }}">
                        <select name="genreSelection">
                            @foreach($genres as $genre)
                                <option value="{{$genre->genre}}">{{$genre->genre}}</option>
                            @endforeach
                        </select>
                        <button type="submit">Go</button>
                    </form>
                </th>
            </tr>
            @if(empty($serieParGenre))
                @foreach($series as $serie)
                    <tr>
                        <td><a href="{{ url("/details{$serie->id}") }}"><img src="{{$serie->urlImage}}"></img></a></td>
                        <td>{{$serie->nom}}</td>
                        <td><a href="{{ url("/trierParGenreClick{$serie->genre}")  }}">{{$serie->genre}} </a></td>
                        <td>{{$serie->langue}}</td>
                        <td>{{$serie->note}}</td>
                    </tr>
                @endforeach
            @endif
            @if(!empty($serieParGenre))

                @foreach($serieParGenre as $spg)
                    <tr>
                        <td><a href="{{ url("/details{$spg->id}") }}"><img src="{{$spg->urlImage}}"></img></a></td>
                        <td>{{$spg->nom}}</td>
                        <td><a href="{{ url("/genre") }}">{{$spg->genre}} </a></td>
                        <td>{{$spg->langue}}</td>
                        <td>{{$spg->note}}</td>
                    </tr>
                @endforeach
            @endif
        </table>
    @else
        <h3>aucune serie</h3>
    @endif
    --}}


@endsection