
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liste des séries</title>

</head>
<body>
<h2>La liste des séries</h2>
@if(!empty($series))
    <table>
        <tr>
            <th></th>
            <th>nom</th>
            <th>genre</th>
            <th>langue</th>
            <th>note</th>
            <th>
                <form method="get" action="{{ route("test") }}">
                    <select name="genreSelection">
                        @foreach($genres as $genre)
                            <option value="{{$genre->genre}}">{{$genre->genre}}</option>
                        @endforeach
                    </select>
                    <button type="submit">Go</button>
                </form>
            </th>
        </tr>
        @if(empty($serieParGenre))
        @foreach($series as $serie)
            <tr>
                <td><a href="{{ url("/details{$serie->id}") }}"><img src="{{$serie->urlImage}}"></img></a></td>
        <td>{{$serie->nom}}</td>
        <td><a href="{{ url("/triParGenre{$serie->genre}") }}">{{$serie->genre}} </a></td>
        <td>{{$serie->langue}}</td>
        <td>{{$serie->note}}</td>
        </tr>
        @endforeach
        @endif
        @if(!empty($serieParGenre))

        @foreach($serieParGenre as $spg)
                <tr>
                    <td><a href="{{ url("/details{$spg->id}") }}"><img src="{{$spg->urlImage}}"></img></a></td>
                    <td>{{$spg->nom}}</td>
                    <td><a href="{{ url("/triParGenre{$spg->genre}") }}">{{$spg->genre}} </a></td>
                    <td>{{$spg->langue}}</td>
                    <td>{{$spg->note}}</td>
                </tr>
        @endforeach
        @endif
    </table>
@else
    <h3>aucune serie</h3>
@endif
@extends('base.base')
@section('content')
    <h2>La liste des séries</h2>
    @if(!empty($series))
        <table>
            <tr>
                <th></th>
                <th>nom</th>
                <th>genre</th>
                <th>langue</th>
                <th>note</th>
            </tr>
            @foreach($series as $serie)
                <tr>
                    <td><a href="{{ url("/details{$serie->id}") }}"><img src="{{$serie->urlImage}}"></a></td>
                    <td>{{$serie->nom}}</td>
                    <td>{{$serie->genre}}</td>
                    <td>{{$serie->langue}}</td>
                    <td>{{$serie->note}}</td>
                </tr>
            @endforeach
        </table>
    @else
        <h3>aucune serie</h3>
    @endif

    {{$series->links('layouts.paginator')}}
@endsection