@extends('base.base')
@section('tittle')
    Enregistrement
@endsection
@section('content')
    <section class="login">

        <div class="form-login register ">
            <form action="{{ route('register') }}" method="post">
                @csrf
                <h1>S'inscrire</h1>
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                <input id="name" type="text"
                       class="form-control @error('name') is-invalid @enderror" name="name"
                       value="{{ old('name') }}" required autocomplete="name" autofocus>
                <label for="email"
                       class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email"
                       class="form-control @error('email') is-invalid @enderror" name="email"
                       value="{{ old('email') }}" required autocomplete="email">
                <label for="password"
                       class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                <input id="password" type="password"
                       class="form-control @error('password') is-invalid @enderror" name="password"
                       required autocomplete="new-password">
                <label for="password-confirm"
                       class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                <input id="password-confirm" type="password" class="form-control"
                       name="password_confirmation" required autocomplete="new-password">
                <div>
                    <button type="submit" class="btn btn-primary btn_validate">
                        {{ __('Register') }}
                    </button>
                </div>
            </form>
        </div>

    </section>
@endsection


