@extends('layouts.app')
@section('content')
    @include('base.nav')
    <ul>
        <h1>Les séries les mieux notées </h1>
        @foreach($series as $serie)
            <li>{{$serie->nom}}</li>
        @endforeach
    </ul>
@endsection