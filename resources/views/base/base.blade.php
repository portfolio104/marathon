<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>WATCHILL - @yield('tittle')</title>
    <link rel="stylesheet" href="{{asset('/css/style.css')}}">
</head>
<body>
@include('base.nav')
@yield('content')
@include('base.footer')
<script type="text/javascript" src="{{asset('/js/script.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vanilla-tilt.js')}}"></script>
</body>
</html>