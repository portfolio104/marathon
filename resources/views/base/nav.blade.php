<nav>
    <ul>
        <li><a id="logo" href="/"><img src="img/icons/logo.svg" alt=""></a></li>
        <li><a href="/">Accueil</a></li>
        @guest
            <li><a href="{{ route('login') }}">Se connecter</a></li>
            <li><a href="{{ route('register') }}">S'enregistrer</a></li>
            <li><a href="{{url("/ListeSerie") }}">Liste des séries</a></li>
        @else
            @if (Auth::user())
                <li><a href="{{url("/profil") }}">Mon Profil</a></li>
            @endif
            <li><a href="/ListeSerie">Liste des séries</a></li>
            <li><a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    Se déconnecter
                </a></li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @endguest

        <form action="{{route('series.search')}}"" >
            <div class="form-group">
                <input type="text" name="query" class="form-control">
            </div>
            <button type="submit" class="btn btn-info">Rechercher</button>
        </form>

        
    </ul>
</nav>



