<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/ListeSerie', [\App\Http\Controllers\SerieController::class,'index'])->name('test');
Route::get('/details{id}',[\App\Http\Controllers\SerieController::class,'show'])->name('afficheDetail');
Route::get('/',[\App\Http\Controllers\AcceuilController::class,'index'])->name('index');

Route::get('/profil', [\App\Http\Controllers\UserController::class,'afficheUser']);

Route::get('/search', [\App\Http\Controllers\RechercheController::class, 'search']) -> name('series.search');


Route::post('/ajoutSerie{id}',[\App\Http\Controllers\VisualiseController::class,'serieVue'])->name('serieVue');
Route::post('/ajoutSerie{id}/{saison}',[\App\Http\Controllers\VisualiseController::class,'saisonVue'])->name('saisonVue');
Route::post('/ajoutSerie{id}/{saison}/{episode}',[\App\Http\Controllers\VisualiseController::class,'EpisodeVue'])->name('episodeVue');
Route::post('/ajoutCommentaire{serie}',[\App\Http\Controllers\SerieController::class,'ajouteCommentaire'])->name('ajoutCommentaire');
Route::get('/genre',[\App\Http\Controllers\SerieController::class,'selectGenre'])->name('test');
Route::get('/valideCommentaire/{id}', [\App\Http\Controllers\SerieController::class,'valideCommentaire'])->name('valideCommentaire');

Route::get("/trierParGenreClick{nom}",[\App\Http\Controllers\SerieController::class,'trieParGenre']);